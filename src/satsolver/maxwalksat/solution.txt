maxwalksat version 20
seed = 2370740
cutoff = 2000
tries = 20
numsol = 1
targetcost = 30
heuristic = best, noise 20 / 100
clauses contain explicit costs
numatom = 120, numclause = 160, numliterals = 358
wff read in
                                           average             average       mean              standard
    lowest     worst    number                when                over      flips                 error
      cost    clause    #unsat    #flips     model   success       all      until        std         of
  this try  this try  this try  this try     found      rate     tries     assign        dev       mean
        24        10        15        20        20       100        20       20.0          *          *
Begin assign with lowest # bad = 24 (atoms assigned true)
 1 3 5 8 14 17 20 25 27 28
 30 31 33 37 39 40 42 44 46 49
 51 52 54 59 62 64 66 68 70 71
 73 75 76 80 82 85 87 89 91 93
 94 96 98 104 106 108 109 113 118 120
End assign

total elapsed seconds = 0.000041
average flips per second = 487804
number of solutions found = 1
mean flips until assign = 20.000000
mean seconds until assign = 0.000041
mean restarts until assign = 1.000000
ASSIGNMENT ACHIEVING TARGET 30 FOUND
