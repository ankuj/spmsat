from collections import Counter as mset
import collections
import copy
import csv  
import gc
from itertools import chain
from itertools import product
import itertools
from math import ceil
import re
import string
from subprocess import call
import os
import time
from collections import OrderedDict
from collections import Counter
import sys
import numbers
import timeit
from collections import defaultdict
import os

""" 
this is the code for the main SPMSAT algorithm change with intermediate states as well as trules growth algorithm applied to sequential action pairs
"""

# declare global variables
constraintVariableSet=set()
constraintCount=1
clauseCount = 0
mainlist=[]
parentArgumentDictionary, predicateInformationConstraintDict, predicateBeforeConstraintDict, predicateInformationConstraintDelDict, operatorInformationConstraintDictionary={},{},{},{},{}
constraintsPerActionDict, predicateDictionary ={},{'pre':[],'add':[],'del':[], 'weight':[]}
KEY0 = None
KEY1 = None
DOMAIN_NAME = None
ACTION_CONSTRAINT1 = None
ACTION_CONSTRAINT2 = None
INFORMATION_CONSTRAINT1 = None
INFORMATION_CONSTRAINT2 = None
DATA_FILE = None
PROBLEM_FILE = None
DOMAIN_FILE = None
MINING_ALGO = None
startTime = None
endTime = None
SOLVER = None
NUMRECORDS, MINING_CONFIDENCE, MINING_SUPPORT = None, None, None
ABSPATH = None


def calculateDeltaIdealObtained(idealModelDictionary, actualModelDictionary, constraintList, predicateTypeDictionary):
#convert the constraints into predicates
    errorDictionary = {}
    for i in idealModelDictionary.keys():
            idealPreList = idealModelDictionary[i]['pre']
            if i.lower() not in actualModelDictionary.keys(): continue
            idealPreList = [j for j in idealPreList if (j in predicateTypeDictionary[i.lower()].keys())]             
            actualPreList = [(re.match(r'(\w+(-\w+)*)\[.*\]', x)).group(1) for x in actualModelDictionary[i.lower()]['pre']]
            # #find out which of the predicates of the actual list are in the ideal list
            errorListPre = [l for l, m in enumerate(actualPreList) if m not in idealPreList]
            errorListSecondPre = [l for l, m in enumerate(idealPreList) if m not in actualPreList]
            idealEffList = idealModelDictionary[i]['eff']
            idealEffList = [m for l,m in enumerate(idealEffList) if ((re.search(r'(not\s\()?(.*)', m)).group(2).strip() in predicateTypeDictionary[i.lower()].keys())]             
            actualEffList = [(re.search(r'((\w*\s*\(\w+(-\w+)*))\[.*\]', x)).group(2).strip() if 'not' in x else (re.search(r'((\w*\s*\w+(-\w+)*))\[.*\]', x)).group(2).strip() for x in actualModelDictionary[i.lower()]['eff'] if 'not']            
            #find out which of the predicates of the actual list are in the ideal list
            errorListEff = [l for l, m in enumerate(actualEffList) if m not in idealEffList]
            errorListSecondEff = [l for l, m in enumerate(idealEffList) if m not in actualEffList]
            #calculate the number of constraints for this particular action in the constraintList
            actionCount = 0
            for j in constraintList:
                if(j.action==i.lower()):
                    actionCount = actionCount + 3
            errorDictionary[i]=((len(errorListPre)+len(errorListSecondPre)+len(errorListEff)+len(errorListSecondEff))/actionCount)
    return errorDictionary

# this method parses the ideal model which is input by the system arguments in order to compare it with the empirical model
def parseIdealModel(idealModelPath):
    with open(idealModelPath, 'r') as idealModelFile:
        idealModelDictionary, actionPredicateDictionary = {},{}
        action = ''
        precondition, effect, negativeEffect = [],[],[]
        for line in idealModelFile:
            if ':action ' in line:
                action = re.search(r':action(.*)', line, re.DOTALL).group(1).strip()
            elif ':precondition' in line:
                precondition = re.findall(r'\((\w+(-\w+)*)\s[\?\w+\s?]+\)', line, re.DOTALL)
                precondition = [list(i)[0] for i in precondition]
                actionPredicateDictionary['pre'] = precondition
            elif ':effect' in line:
                negativeEffect = re.findall(r'((not\s\(\w+(-\w+)*)\s[\?\w+\s?]+\))', line, re.DOTALL)
                negativeEffect = [list(i)[1] for i in negativeEffect]                
                for i in negativeEffect:
                    line = line.replace(i, '')
                effect = re.findall(r'\((\w+(-\w+)*)\s[\?\w+\s?]+\)', line, re.DOTALL)
                effect = [list(i)[0] for i in effect]                
                actionPredicateDictionary['eff'] = effect+negativeEffect
            if(action and precondition and effect):
                idealModelDictionary[action] = actionPredicateDictionary
                action = ''
                precondition, effect, negativeEffect = [],[],[]
                actionPredicateDictionary = {}
        idealModelFile.close()
        return idealModelDictionary   

def writeInformationConstraintsToFile(constraintList, predicateInformationConstraintDict, constraintType, constraintWriteFile, actionDictionary, operatorAllCombinationsDictionary):
        print('In method writeInformationConstraintsToFile()')
        global constraintCount, clauseCount
        for key in predicateInformationConstraintDict.keys():
            constraintString = ''
            match = (re.search(r'(.*)\[(.*)\]', key, re.DOTALL))
            for j in predicateInformationConstraintDict[key]:
                for i in constraintList:
                    constraintExists = False
                    if((i.action == j) and (i.predicate == match.group(1)) and (str(i.arguments).replace('[','').replace(']','') == match.group(2))):
                        #add the found predicate to the constraint string which is to be added to the file later down the line
                        if(constraintType=='add'):
                            constraintString = constraintString + ' '+str(i.AddConstraintNumber)
                        elif(constraintType=='before'):
                            constraintString = constraintString + ' '+str(i.PreConstraintNumber)
                        #below mentioned block seems to be wrongly done. it is adding delete constraints for all the actions whereas it should only be for the last action
                        elif(constraintType=='del'):
                            constraintWriteFile.write(str(INFORMATION_CONSTRAINT2)+' '+'-'+str(i.DelConstraintNumber)+' 0'+'\n')
                            constraintCount = constraintCount + 1
                        constraintExists = True
                        break
                if not constraintExists:
                    #create variables for them to be added to the constraint class 
                    alreadyAddedList = []
                    variableForPredicate = []
                    for x in match.group(2).split(','):
                        x = x.replace('"','').replace("'","").strip()
                        count = 0
                        if x in alreadyAddedList:
                            count = count + 1
                        variableForPredicate.append(x[0]+x[1]+str(count)+' - '+x)
                        alreadyAddedList.append(x) 
                    #match the action signature with the predicate to check if the predicate can be applied at all or not to the action. This part will now be changed to find a match with operatorAllCombinationsDictionary
                    testNew = []
                    for key in operatorAllCombinationsDictionary[j]:
                        testNew.append(len([l for l, m in enumerate(key) if (m in alreadyAddedList)]))
                    #remove this block of code at the moment to test the possibility of not adding new constraints and seeing what impact it has on the result                                              
                    if(max(testNew)>=len(alreadyAddedList)):                                      
                        newClass= constraintClass(j, match.group(1), alreadyAddedList, variableForPredicate, constraintList[len(constraintList)-1].getConstraintNumber())                                         
                        constraintList.append(newClass)
                        if(constraintType=='add'):
                            constraintString = constraintString + ' '+ str(newClass.AddConstraintNumber)
                        elif(constraintType=='before'):
                            constraintString = constraintString + ' '+ str(newClass.PreConstraintNumber)                            
                        elif(constraintType=='del'):
                            constraintWriteFile.write(str(INFORMATION_CONSTRAINT2)+' '+'-'+str(newClass.DelConstraintNumber) +' 0'+'\n')
                            constraintCount = constraintCount + 1 
##END OF COMMENTING                                                                                                                       
            if((constraintType=='add' or constraintType=='before') and constraintString!=''):
                constraintWriteFile.write(str(INFORMATION_CONSTRAINT1)+constraintString + ' 0'+'\n')
                constraintCount = constraintCount + 1
                clauseCount = clauseCount + 1                

                
# this method writes the PDDL file for the constraints solved to create the empirical model
def createPDDLFile(modelReconstructionDictionary, stateDictionary, actionDictionary, constraintList, parentArgumentDictionary):
    typeLine=''
    alreadyWritten=False
    actualModelDictionary = {}
    with open(str(ABSPATH)+ '/generatedFiles/PDDLDomainFile.txt', 'w') as PDDLWriteFile:
        PDDLWriteFile.write('(define (domain depots)\n')
        PDDLWriteFile.write('(\t:requirements :strips :typing)\n')
        PDDLWriteFile.write('(:types')
        typeLine=''
        for i in mainlist[0::2]:
            typeLine=typeLine+' '+i
        typeLine=typeLine+' - '+'object'
        PDDLWriteFile.write(typeLine)
        typeLine=''
        for key in parentArgumentDictionary.keys():
            for i in parentArgumentDictionary[key]:
                typeLine=typeLine+' '+i
            typeLine=typeLine+' - '+key
            typeLine=''
            PDDLWriteFile.write(typeLine+')\n')
            
        PDDLWriteFile.write(')\n(:predicates ')
        #write down the predicates
        count=0
        for key in stateDictionary:
            typeLine='('            
            typeLine=typeLine+key+' '
            for i in stateDictionary[key]:
                for j in i:
                    alreadyWritten=False 
                    if(len(i)!=len(set(i))):
                        typeLine=typeLine+'?'+j[0]+j[1]+j[2]+str(count)+' - '+j+' '
                        count=count+1
                    else:
                        typeLine=typeLine+'?'+j[0]+j[1]+j[2]+'0'+' - '+j+' '
                if(len(stateDictionary[key])>1):
                    PDDLWriteFile.write(typeLine+'\n')
                    alreadyWritten=True
                    typeLine=''                    
                    typeLine=typeLine+key+' '
            if not alreadyWritten:
                PDDLWriteFile.write(typeLine+')\n')
        PDDLWriteFile.write(')\n')
        for i in modelReconstructionDictionary.keys():
            typeLine=''
            preList, effectList = [],[]
            innerDictionary = {}
            PDDLWriteFile.write('\n (:action '+' '+i+'\n')
            typeLine=typeLine+':parameters('
            for j in actionDictionary[i]:
                typeLine=typeLine+' '+'?'+j[0]+j[1]+j[2]+str(0)+' - '+j
            PDDLWriteFile.write(typeLine+')'+'\n')
            typeLine=''    
            PDDLWriteFile.write(':precondition (and ')
            for j in constraintList:
                for k in list(set(modelReconstructionDictionary[i]['pre'])):
                    if(j.PreConstraintNumber==k):
                        #take out the name of the variable from the entire label
                        snippedVariableNames = re.findall(r'(\w+)\s-', str(j.variableNames))
                        preList.append(j.predicate+str(j.variableNames))
                        variableString = '' 
                        for z in snippedVariableNames:
                            variableString = variableString +' '+'?'+z                        
                        typeLine=typeLine+' ('+j.predicate+' '+variableString+')'
            typeLine=typeLine+')'
            innerDictionary['pre'] = preList
            PDDLWriteFile.write(typeLine+'\n')
            typeLine=''    
            PDDLWriteFile.write(':effect (and ')
            for j in constraintList:
                for k in list(set(modelReconstructionDictionary[i]['del'])):
                    if(j.DelConstraintNumber==abs(k)):
                        snippedVariableNames = re.findall(r'(\w+)\s-', str(j.variableNames))
                        variableString = ''
                        for z in snippedVariableNames:
                            variableString = variableString + ' '+'?'+z
                        typeLine=typeLine+' (not ('+j.predicate + variableString+')'
                        effectList.append(' not ('+j.predicate+str(j.variableNames)+') ')
                        typeLine = typeLine + ')'
                for k in list(set(modelReconstructionDictionary[i]['add'])):
                    if(j.AddConstraintNumber==abs(k)):
                        snippedVariableNames = re.findall(r'(\w+)\s-', str(j.variableNames))
                        variableString = ''
                        for z in snippedVariableNames:
                            variableString = variableString + ' ' +'?'+z
                        typeLine=typeLine+' ('+j.predicate+' '+variableString+')'
                        effectList.append(' ('+j.predicate+str(j.variableNames)+') ')                        
            typeLine=typeLine+')'
            innerDictionary['eff'] = effectList
            actualModelDictionary[i] = innerDictionary
            PDDLWriteFile.write(typeLine+')'+'\n')
        PDDLWriteFile.write('\n)\n')
    PDDLWriteFile.close()
    return actualModelDictionary
    
    
# this method invokes the satsolver to solve the constraints previously generated    
def reconstructModelFromConstraints(constraintList):
    constraintNumberList = []
    for i in constraintList:
        constraintNumberList.append(i.getConstraintNumber())
    
    global DOMAIN_NAME
#     os.system("."+str(ABSPATH)+('/src/satsolver/wmaxsat')+' '+str(ABSPATH)+"/src/generatedFiles/constraints"+str(DOMAIN_NAME)+".wcnf > " +str(ABSPATH) + "/src/generatedFiles/constraintsOutput.txt")
    os.system("./satsolver/wmaxsat "+str(ABSPATH)+"/generatedFiles/constraints"+str(DOMAIN_NAME)+".wcnf > "+str(ABSPATH)+"/generatedFiles/constraintsOutput.txt")
    time.sleep(2) 
    #read the constraints which are satisfied from the constraint output file produced from the MAX-SAT solver
    satisfiedConstraints = []
    global predicateDictionary
    with open(str(ABSPATH)+ '/generatedFiles/constraintsOutput.txt','r') as maxsatOutputFile:
        count=1
        for line in maxsatOutputFile:
            if "Best solution:" in line and count <= max(constraintNumberList):            
                splitLine=line.split(' ')
                if(splitLine[2].strip(' \t\n\r')=='1'):
                    satisfiedConstraints.append(count)
                count=count+1
    maxsatOutputFile.close()
    modelReconstructionDictionary={}    
    #get the weights of the satisfied constraints as well
    for i in constraintList:
        for j in satisfiedConstraints:
            if(i.PreConstraintNumber==j):
                predicateDictionary['pre'].append(i.PreConstraintNumber)
                if i.action not in modelReconstructionDictionary.keys():
                    modelReconstructionDictionary[i.action]=predicateDictionary
                else:
                    modelReconstructionDictionary[i.action]['pre'].append(i.PreConstraintNumber)
            if(i.AddConstraintNumber==j):
                predicateDictionary['add'].append(i.AddConstraintNumber)
                if i.action not in modelReconstructionDictionary.keys():
                    modelReconstructionDictionary[i.action]=predicateDictionary
                else:
                    modelReconstructionDictionary[i.action]['add'].append(i.AddConstraintNumber)
            if(i.DelConstraintNumber==j):
                predicateDictionary['del'].append(i.DelConstraintNumber)
                if i.action not in modelReconstructionDictionary.keys():
                    modelReconstructionDictionary[i.action]=predicateDictionary
                else:
                    modelReconstructionDictionary[i.action]['del'].append(i.DelConstraintNumber)
            predicateDictionary={'pre':[],'add':[],'del':[], 'weight':[]}
    return modelReconstructionDictionary

# this method invokes the maxwalksat solver to solve the constraints previously generated    
def reconstructModelFromConstraintsMaxWalkSat(constraintList):
    os.system("./satsolver/maxwalksat/maxwalksat -targetcost 30 -noise 20 100 -cutoff 2000 -tries 20 -sol< " +str(ABSPATH) + "/generatedFiles/constraints"+str(DOMAIN_NAME)+".wcnf > "+str(ABSPATH)+"/generatedFiles/solutionMWalkSat")
    time.sleep(2) 
#read the constraints which are satisfied from the constraint output file produced from the MAX-SAT solver
    satisfiedConstraints, indexSatisfiedConstraints=[],[]
    global predicateDictionary
    with open(str(ABSPATH) + '/generatedFiles/solutionMWalkSat','r') as maxsatOutputFile:
        wholeFile  = maxsatOutputFile.read()
        substring = wholeFile[wholeFile.find('true)')+6:wholeFile.find('End assign')]
        count=1
    satisfiedConstraints =[int(s) for s in substring.split() if s.isdigit()]
    maxsatOutputFile.close()
    modelReconstructionDictionary={}    
#get the weights of the satisfied constraints as well
    for i in constraintList:
        for j in satisfiedConstraints:
            if(i.PreConstraintNumber==j):
                predicateDictionary['pre'].append(i.PreConstraintNumber)
                if i.action not in modelReconstructionDictionary.keys():
                    modelReconstructionDictionary[i.action]=predicateDictionary
                else:
                    modelReconstructionDictionary[i.action]['pre'].append(i.PreConstraintNumber)
            if(i.AddConstraintNumber==j):
                predicateDictionary['add'].append(i.AddConstraintNumber)
                if i.action not in modelReconstructionDictionary.keys():
                    modelReconstructionDictionary[i.action]=predicateDictionary
                else:
                    modelReconstructionDictionary[i.action]['add'].append(i.AddConstraintNumber)
            if(i.DelConstraintNumber==j):
                predicateDictionary['del'].append(i.DelConstraintNumber)
                if i.action not in modelReconstructionDictionary.keys():
                    modelReconstructionDictionary[i.action]=predicateDictionary
                else:
                    modelReconstructionDictionary[i.action]['del'].append(i.DelConstraintNumber)
            predicateDictionary={'pre':[],'add':[],'del':[], 'weight':[]}
    return modelReconstructionDictionary


# class that represents constraints
class constraintClass:
    constraintNumberGlobal=0
    def __init__(self, action, predicate, arguments, variableNames, constraintNumber):
        self.action=action
        self.predicate=predicate
        self.arguments=arguments
        self.variableNames=variableNames
        self.PreConstraintNumber=constraintNumber
        constraintNumber=constraintNumber+1
        self.AddConstraintNumber=constraintNumber
        constraintNumber=constraintNumber+1
        self.DelConstraintNumber=constraintNumber
        constraintNumber=constraintNumber+1
        self.constraintNumberGlobal=constraintNumber
    def getConstraintNumber(self):
        return self.constraintNumberGlobal

# this function converts predicates relevant to each action into constraints
def convertPredicatesToConstraints(predicateTypeDictionary):
    global constraintCount
    constraintList=[]
    for i in sorted(predicateTypeDictionary.keys()):
        for j in sorted(predicateTypeDictionary[i].keys()):
            count=0
            for k in (predicateTypeDictionary[i][j][0::2]):
                if(not(isinstance(k, list))):
                      insertionPredicate=predicateTypeDictionary[i][j]
                      notList=True
                else:
                     insertionPredicate=k
                     notList=False
                classCreation=constraintClass(i,j,insertionPredicate, predicateTypeDictionary[i][j][2*count+1], constraintCount)
                count=count+1
                constraintList.append(classCreation)
                constraintCount=classCreation.getConstraintNumber()
                if(notList):
                    break
    return constraintList, constraintCount


# this function creates the plan constraints
def modelPlanConstraints(actionDictionary, constraintList, predicateTypeDictionary, informationConstraintDictionary, informationConstraintCount, maxConstraintCount):
    firstActionList, secondActionList=[],[]
    global DOMAIN_NAME, MINING_CONFIDENCE, MINING_SUPPORT, MINING_ALGO, clauseCount, constraintCount
    count=0
    matchFound=False
    print('in modelPlanConstraints function')
    actionDictCount = 1
    actionNumberedDictionary = {}
    actionList = list(actionDictionary.keys())
    actionList.sort()
    for key in actionList:
        actionNumberedDictionary[key] = actionDictCount
        actionDictCount = actionDictCount + 1 
    #choose between the apriori algorithm and the tRuleGrowth algorithm here
    if(MINING_ALGO == 'apriori'):createAprioriFile(actionNumberedDictionary)
    else: createTRulesGrowthFile(actionNumberedDictionary)  
    #these constraints need to be modeled for all the predicates in the constraint list
    with open(str(ABSPATH) + '/generatedFiles/constraints'+str(DOMAIN_NAME)+'.wcnf', 'a') as constraintWriteFile:
        actionPredicateDict, predicateArgumentDict={}, {}
        if MINING_ALGO == 'trulegrowth': os.system("java -jar " +str(ABSPATH) + "/spmf/spmf.jar run TRuleGrowth " +str(ABSPATH) + "/generatedFiles/newTrule"+ DOMAIN_NAME +".txt "+str(ABSPATH)+ "/generatedFiles/"+MINING_ALGO+"Output"+DOMAIN_NAME+" "+ str(MINING_CONFIDENCE) + " " + str(MINING_SUPPORT)+" 1")
        elif MINING_ALGO == 'apriori': os.system("java -jar " +str(ABSPATH) + "/spmf/spmf.jar run Apriori "+str(ABSPATH)+"/generatedFiles/newAprioriRule"+ DOMAIN_NAME +".txt " +str(ABSPATH)+ "/generatedFiles/aprioriOutput"+DOMAIN_NAME+" "+str(MINING_SUPPORT))
        time.sleep(2) 
        with open(str(ABSPATH) +'/generatedFiles/'+MINING_ALGO+'Output'+(str(DOMAIN_NAME)).lower(), 'r') as aprioriFile:
            for row in aprioriFile:
                splitRow=row.split(' ')
                if(len(splitRow)==5 or len(splitRow)==7):
                    if(len(splitRow)==5):support=int(splitRow[4].strip())
                    if(len(splitRow)==7):support=int(splitRow[4].strip())
                    if(support>0):                    
                        firstActionList=[]
                        secondActionList=[]
                        firstAction=list(actionNumberedDictionary.keys())[list(actionNumberedDictionary.values()).index(int(splitRow[0].strip()))]
                        if(len(splitRow)==5):
                            secondAction=list(actionNumberedDictionary.keys())[list(actionNumberedDictionary.values()).index(int(splitRow[1].strip()))]
                        else:
                            secondAction=list(actionNumberedDictionary.keys())[list(actionNumberedDictionary.values()).index(int(splitRow[2].strip()))]
                        for i in constraintList:
                            #find out all the ones that correspond to the first and to the second actions 
                            if(i.action==firstAction):
                                #see if new predicate and argument being added already exists in the action list or not  
                                if(firstActionList==[]):  
                                   firstActionList.append(i) 
                                else: 
                                   matchFound=False 
                                   for t in firstActionList:
                                       if (t.action==i.action and t.predicate==i.predicate and t.arguments==i.arguments):
                                           matchFound=True
                                           break
                                   if not matchFound:
                                        firstActionList.append(i)
                                        matchFound=False
                            if(i.action==secondAction):
                                #see if new predicate and argument being added already exists in the action list or not  
                                if(secondActionList==[]):  
                                   secondActionList.append(i) 
                                else: 
                                   matchFound=False 
                                   for t in secondActionList:
                                       if (t.action==i.action and t.predicate==i.predicate and t.arguments==i.arguments):
                                           matchFound=True
                                           break
                                   if not matchFound:
                                       secondActionList.append(i)
                        for i in firstActionList:
                            for j in secondActionList:
                                if(i.predicate==j.predicate and i.arguments==j.arguments):
                                    #add the constraints specific to the first action                                
                                    constraintWriteFile.write(str(support)+' '+str(i.PreConstraintNumber)+' '+str(i.DelConstraintNumber)+' '+str(j.PreConstraintNumber)+' 0'+'\n')
                                    constraintWriteFile.write(str(support)+' '+str(i.DelConstraintNumber)+' '+str(j.PreConstraintNumber)+' 0'+'\n')
                                    constraintWriteFile.write(str(support)+' '+str(i.PreConstraintNumber)+' '+str(i.AddConstraintNumber)+' '+str(i.DelConstraintNumber)+' 0'+'\n')
                                    constraintWriteFile.write(str(support)+' '+str(i.AddConstraintNumber)+' '+str(i.DelConstraintNumber)+' '+str(j.PreConstraintNumber)+' 0'+'\n')
                                    constraintWriteFile.write(str(support)+' '+str(i.PreConstraintNumber)+' '+str(i.AddConstraintNumber)+' '+str(j.AddConstraintNumber)+' 0'+'\n')
                                    constraintWriteFile.write(str(support)+' '+str(i.AddConstraintNumber)+' -'+str(i.DelConstraintNumber)+' '+str(j.AddConstraintNumber)+' 0'+'\n')
                                    constraintWriteFile.write(str(support)+' '+str(i.AddConstraintNumber)+' '+str(j.PreConstraintNumber)+' '+str(j.AddConstraintNumber)+' 0'+'\n')
                                    constraintWriteFile.write(str(support)+' '+str(i.PreConstraintNumber)+' '+str(j.PreConstraintNumber)+' '+str(j.AddConstraintNumber)+' 0'+'\n')
                                    constraintWriteFile.write(str(support)+' -'+str(i.DelConstraintNumber)+' '+str(j.PreConstraintNumber)+' '+str(j.AddConstraintNumber)+' 0'+'\n')                                    
                                    constraintWriteFile.write(str(support)+' '+str(j.PreConstraintNumber)+' '+str(j.AddConstraintNumber)+' 0'+'\n')                                                                                                                                                                                    
                                    constraintVariableSet.add(i.PreConstraintNumber)
                                    constraintVariableSet.add(i.AddConstraintNumber)
                                    constraintVariableSet.add(i.DelConstraintNumber)
                                    constraintVariableSet.add(j.PreConstraintNumber)
                                    constraintVariableSet.add(j.AddConstraintNumber)
                                    constraintVariableSet.add(j.DelConstraintNumber)                                                                                                                                                                                    
                                    constraintCount=constraintCount+10
                                    clauseCount = clauseCount + 10                                                                                                                                             
        constraintWriteFile.close() 
    return constraintList

def modelActionConstraints(constraintList, informationConstraintDictionary, informationConstraintCount, maxConstraintCount):
#these constraints need to be modeled for all the predicates in the constraint list
    global DOMAIN_NAME, constraintCount, clauseCount
    with open(str(ABSPATH) + '/generatedFiles/constraints'+str(DOMAIN_NAME)+'.wcnf','a') as constraintWriteFile:
        for x in constraintList:
            constraintWriteFile.write(str(ACTION_CONSTRAINT1)+' '+str(-(x.PreConstraintNumber))+' '+str(-(x.AddConstraintNumber))+' '+str(0)+'\n')
            constraintWriteFile.write(str(ACTION_CONSTRAINT2)+' '+str(-(x.DelConstraintNumber))+' '+str((x.PreConstraintNumber))+' '+str(0)+'\n')
            constraintVariableSet.add(x.PreConstraintNumber)
            constraintVariableSet.add(x.AddConstraintNumber)
            constraintVariableSet.add(x.DelConstraintNumber)                        
            constraintCount=constraintCount+2
            clauseCount = clauseCount + 2            
    return constraintList

def modelInformationConstraints(constraintList,predicateTypeDictionary, informationConstraintDictionary, informationConstraintCount, maxConstraintCount, actionDictionary, operatorAllCombinationsDictionary):
    print('in method modelInformationConstraints')
    global DOMAIN_NAME, constraintCount, predicateInformationConstraintDict, predicateInformationConstraintDelDict, operatorInformationConstraintDictionary, MINING_ALGO, clauseCount
    existingConstraintsInner=dict()
    existingConstraints=dict()
    matchFound, constraintExists = False, False
    constraintNumberList, insertionPredicate, variableForPredicate=[],[],[]
    #introduce all the previously calculated constraints    
    with open(str(ABSPATH) + '/generatedFiles/constraints'+str(DOMAIN_NAME)+'.wcnf','w') as constraintWriteFile:
        writeInformationConstraintsToFile(constraintList, predicateInformationConstraintDict, 'add', constraintWriteFile, actionDictionary, operatorAllCombinationsDictionary)
#         writeInformationConstraintsToFile (constraintList, predicateInformationConstraintDelDict, 'del', constraintWriteFile, actionDictionary, operatorAllCombinationsDictionary)
        if MINING_ALGO == 'trulegrowth': writeInformationConstraintsToFile (constraintList, predicateBeforeConstraintDict, 'before', constraintWriteFile, actionDictionary, operatorAllCombinationsDictionary)
#figure out the constraints which are not in the constraint list, and add them as new constraints and to the constraint file as well
#need to check whether the constraints coming in are already present in the constraint dictionary, if not add the new one
        for i in informationConstraintDictionary:
            for j in informationConstraintDictionary[i].keys():
                #figure out the probable info constraints which are not already numerised in the existing constraint dictionary, and number them
                if j in predicateTypeDictionary[i].keys():
                   for k in informationConstraintDictionary[i][j]:
                       #sort both the lists before the comparison
                        k.sort()
                        for x in predicateTypeDictionary[i][j][0::2]:
                            x.sort()                       
                        if  (isinstance(k, list) and (k in predicateTypeDictionary[i][j][0::2])):
#                        if k in predicateTypeDictionary[i].values():
                            existingConstraintsInner[j]=k  
            existingConstraints[i]=existingConstraintsInner
            existingConstraintsInner={}
#logic to find out where the constraints belong in the constraint list, and code them into the cnf file
#Also figure out the ones which are not in the existing constraints, and represent them and add them to the cnf file
        for i in informationConstraintDictionary:
            for j in informationConstraintDictionary[i].keys():
                for k in (informationConstraintDictionary[i][j]):
                    if(not(isinstance(k, list))):
                          insertionPredicate=informationConstraintDictionary[i][j]
                          notList=True
                    else:
                         insertionPredicate=k
                         notList=False
#figure out if these predicates are applicable to that particular action or not                     
                    for x in constraintList:
                        if((i==x.action) and (j==x.predicate) and ((insertionPredicate==x.arguments) or set(insertionPredicate)==set(x.arguments))):
                            constraintNumberList.append(x.getConstraintNumber())
#                             constraintWriteFile.write(str(informationConstraintCount[i])+str(-(x.PreConstraintNumber))+' '+str(-(x.AddConstraintNumber))+' '+str(0))
#                             constraintWriteFile.write(str(informationConstraintCount[i])+str(-(x.DelConstraintNumber))+' '+str((x.PreConstraintNumber))+' '+str(0))
#count the number of variables for the constraint file count
                            constraintVariableSet.add(x.PreConstraintNumber)
#                             print('###Information Constraints###')
#                             print(' '+str(x.predicate)+' '+str(x.arguments) +'belongs to pre-list of action: '+str(x.action))
                            constraintWriteFile.write(str(informationConstraintCount[i])+' '+str((x.PreConstraintNumber))+' '+str(0)+'\n')
                            print()
                            if not (x.action in constraintsPerActionDict.keys()):
                                constraintsPerActionDict[x.action]=[str(informationConstraintCount[i])+' '+str((x.PreConstraintNumber))+' '+str(0)]
                            else:
                                constraintsPerActionDict[x.action].append(str(informationConstraintCount[i])+' '+str((x.PreConstraintNumber))+' '+str(0))
                            constraintCount=constraintCount+1
                            clauseCount = clauseCount + 1
#add the clauses for the predicates which can't be found in the existing constraint set
                            matchFound=True                            
                            break
                    if(not(matchFound)):
                        variableForPredicate = []
                        count=0
                        for tt in insertionPredicate:
                            variableForPredicate.append((tt)[0]+(tt)[1]+str(count)+' - '+tt)
#test to remove code which adds new constraints to see if it changes the result or not                            
                        classCreation=constraintClass(i,j,insertionPredicate, variableForPredicate, maxConstraintCount)
                        constraintVariableSet.add(classCreation.PreConstraintNumber)
                        constraintWriteFile.write(str(informationConstraintCount[i])+' '+str((classCreation.PreConstraintNumber))+' '+str(0)+'\n')
                        if not (classCreation.action in constraintsPerActionDict.keys()):
                            constraintsPerActionDict[classCreation.action]=[str(informationConstraintCount[i])+' '+str((classCreation.PreConstraintNumber))+' '+str(0)]
                        else:
                            constraintsPerActionDict[classCreation.action].append(str(informationConstraintCount[i])+' '+str((classCreation.PreConstraintNumber))+' '+str(0))
                        constraintCount=constraintCount+1
                        clauseCount = clauseCount + 1                        
                        maxConstraintCount=classCreation.getConstraintNumber()
                        constraintList.append(classCreation)
#COMMENTING ENDS HERE                        
                    matchFound=False
    return constraintList    





    
#The idea of this function is to find all the types of the objects which are similar to each other.
# We can compare the action names and arguments for a single trace, starting with the signatures where only
# one of the arguments are different. This allows to find the single argument which is different and match the types.
def findVariablesSameType(actionDictionary, splitActionToArguments,sameTypeList):
        for i in range(0,len(actionDictionary)):
            if(not(actionDictionary[i]==splitActionToArguments[i])):
                if(not((actionDictionary[i] in sameTypeList) and (splitActionToArguments[i] in sameTypeList))):
                    sameTypeList.append(actionDictionary[i])
                    sameTypeList.append(splitActionToArguments[i])
        return sameTypeList
def matchAllActions(actionPredicateMatchList,element):
        print('in method matchAllActions()')
        for i in range(len(actionPredicateMatchList)):
    #            [ True for x, y in enumerate(element) if (''.join(y[1])).find(actionPredicateMatchList[i][0])]
    #        x=any(actionPredicateMatchList[i][0] in x for x in element)
    #        matchedPredicate=actionPredicateMatchList[i][0] in [l for k in element for j in k for l in j]
            matchedPredicate=list(l for k in element for j in k for l in j)
    #        matchedPredicate=re.search(r'-*'+actionPredicateMatchList[i][0]+r'',''.join(x))
            match=(actionPredicateMatchList[i][0] in ''.join(matchedPredicate)) 
            if(not(match)):
                return False
    #            if(element.index(actionPredicateMatchList[i][0])):
        return True
        
    
def modelAllConstraints(predicateActionMatchDictionary, actionDictionary, stateDictionary, predicateTypeDictionary, informationConstraintDictionary, informationConstraintCount, parentArgumentDictionary, operatorAllCombinationsDictionary):
    print('in method modelAllConstraints()')
    global DOMAIN_NAME, startTime, SOLVER, clauseCount
    #step to label the predicates of each action into individual numbers. Consider each predicate instance in an action as an individual constraint
    constraintList, maxConstraintCount=convertPredicatesToConstraints(predicateTypeDictionary)
    #delete previous constraint file if it exists
    try: os.remove(str(ABSPATH) + '/generatedFiles/constraints'+str(DOMAIN_NAME)+'.wcnf')
    except OSError: pass
    constraintList = modelInformationConstraints(constraintList, predicateTypeDictionary, informationConstraintDictionary, informationConstraintCount, maxConstraintCount, actionDictionary, operatorAllCombinationsDictionary)
    constraintList = modelActionConstraints(constraintList, informationConstraintDictionary, informationConstraintCount, maxConstraintCount)
    constraintList = modelPlanConstraints(actionDictionary, constraintList, predicateTypeDictionary, informationConstraintDictionary, informationConstraintCount, maxConstraintCount)
    #print all the constraints formed till now
    print('All the constraints generated are as follows:')
    for i in constraintList:
        print('i.PreConstraintNumber', i.PreConstraintNumber,'i.AddConstraintNumber', i.AddConstraintNumber, 'i.DelConstraintNumber', i.DelConstraintNumber, 'i.action', i.action, 'i.predicate', i.predicate,'i.arguments', i.arguments, 'i.variableNames', i.variableNames)
    maxConstraintNumber = [i.DelConstraintNumber for i in constraintList]
    #write to the constraint file here
    with open(str(ABSPATH) + '/generatedFiles/constraints'+str(DOMAIN_NAME)+'.wcnf', 'r+') as f:
        content = f.read()
        f.seek(0, 0)
#         f.write('p wcnf ' + str(max(maxConstraintNumber)) +' '+str(constraintCount)+ '\n' + content)
        f.write('p wcnf ' + str(max(maxConstraintNumber)) +' '+str(clauseCount)+ '\n' + content)
        f.close()
    if str(SOLVER)=='maxsat': modelReconstructionDictionary = reconstructModelFromConstraints(constraintList)
    elif str(SOLVER)=='maxwalksat': modelReconstructionDictionary = reconstructModelFromConstraintsMaxWalkSat(constraintList)
    actualModelDictionary = createPDDLFile(modelReconstructionDictionary, stateDictionary, actionDictionary, constraintList, parentArgumentDictionary)
    idealModelDictionary = parseIdealModel(DOMAIN_FILE)                
    errorDictionary = calculateDeltaIdealObtained(idealModelDictionary, actualModelDictionary, constraintList, predicateTypeDictionary)
    print('cumulative error for model: ', sum(errorDictionary.values())/len(errorDictionary.keys()))
    endTime = timeit.default_timer()
    print('running time of program', endTime - startTime )
    print('thats all folks!')
        
# this method translates the traces into a format that can be read by the spmf library        
def createAprioriFile(actionNumberedDictionary):
        with open(str(ABSPATH) + "/generatedFiles/labelledTraces.data","r") as csvFile:
            global predicateInformationConstraintDict, predicateInformationConstraintDelDict, DOMAIN_NAME, NUMRECORDS
            target = open(str(ABSPATH) + "/generatedFiles/aprioriFile"+str(DOMAIN_NAME)+".txt", 'w')
            print('in method createAprioriFile()')
            count=0
            inreader=csv.reader(csvFile, delimiter=';')
            for row in inreader:
                count=count+1
                if(count > NUMRECORDS):break
#                 print('on row number: ', count)
                splitActions=(re.search('\[\s?(.*),?\s?\]', row[0], re.DOTALL)).group(1).split(',') #split each action in to its action name and arguments
    #save information regarding the predicate not being deleted by the last action                                    
                tempActionPredicateList = []
                for j in range(0,len(splitActions)):
                    splitActionToArguments=splitActions[j].split()
                    match=re.search('\((.*)', splitActionToArguments[0], re.DOTALL)
    #to inject an information constraint, the first action must be taken into account. Take into account the first action of every trace
                    if(match):
    #if we are considering the first action of the trace, then it must be accounted for                    
                        target.write(match.group(1))
                        target.write(" ")
                target.write('\n')
        target.close()
        with open(str(ABSPATH) + "/generatedFiles/aprioriFile"+str(DOMAIN_NAME)+".txt", "r") as infile:
        #create the negations of each of the predicates in the dictionary
        #If arguments if the same type are in the action, multiple predicates can be added in that case
        #First figure out if many arguments in the action are of the same type
            i=0
            target = open(str(ABSPATH) + '/generatedFiles/newAprioriRule'+(str(DOMAIN_NAME)).lower()+'.txt', 'w')
            for line in infile:
                for key in sorted(actionNumberedDictionary.keys(), reverse = True):
                    line=str.replace(line, key, str(actionNumberedDictionary[key]))
                target.write(line)
            numberAction=0
        target.close()
        infile.close()
        
# this method translates the traces into a format that can be read by the spmf library with the tRuleGrowth algorithm       
def createTRulesGrowthFile(actionNumberedDictionary):
    global NUMRECORDS    
    with open(str(ABSPATH) +"/generatedFiles/labelledTraces.data","r") as csvFile:
        global predicateInformationConstraintDict, predicateInformationConstraintDelDict, DOMAIN_NAME
        target = open(str(ABSPATH) +"/generatedFiles/tRuleGrowth"+str(DOMAIN_NAME)+".txt", 'w')
        print('in method createTRulesGrowthFile()')
        count=0
        inreader=csv.reader(csvFile, delimiter=';')
        for row in inreader:
            count=count+1
            if(count > NUMRECORDS):break
            splitActions=(re.search('\[\s?(.*),?\s?\]', row[0], re.DOTALL)).group(1).split(',')#split each action in to its action name and arguments
#save information regarding the predicate not being deleted by the last action                                    
            tempActionPredicateList = []
            for j in range(0,len(splitActions)):
                splitActionToArguments=splitActions[j].split()
                match=re.search('\((.*)', splitActionToArguments[0], re.DOTALL)
#to inject an information constraint, the first action must be taken into account. Take into account the first action of every trace
                if(match):
#if we are considering the first action of the trace, then it must be accounted for                    
                    target.write(match.group(1))
                    target.write(" -1 ")
            target.write('-2')                    
            target.write('\n')
    target.close()
    with open(str(ABSPATH) +"/generatedFiles/tRuleGrowth"+str(DOMAIN_NAME)+".txt", "r") as infile:
    #create the negations of each of the predicates in the dictionary
    #If arguments if the same type are in the action, multiple predicates can be added in that case
    #First figure out if many arguments in the action are of the same type
        i=0
        target = open(str(ABSPATH) +'/generatedFiles/newTrule'+str(DOMAIN_NAME)+'.txt', 'w')
        for line in infile:
            for key in sorted(actionNumberedDictionary.keys(), reverse = True):
                line=str.replace(line, key, str(actionNumberedDictionary[key]))
            target.write(line)
        numberAction=0
    infile.close()
    target.close()

#This function reads the traces one by one and creates constructs which later help in the construction of constraints
def scanTraces():
    with open(str(ABSPATH) + "/generatedFiles/labelledTraces.data","r") as csvFile:
        global predicateInformationConstraintDict, predicateInformationConstraintDelDict, NUMRECORDS
        print('in method scanTraces()')
        stateArgumentsList, operatorSequenceList, unitaryOperatorList, actionArgumentsList, sameTypeList, operatorSequenceInnerList = [],[],[],[],[],[]
        tempActionPredicateList=[]
        # the informationConstraint dictionary has all the information specific to information constraints
        informationConstraintDictionary = defaultdict(dict)
        operatorAllCombinationsDictionary, predicateInformationConstraintDictCount = {},{}
        variableTypePerPredicate=[]
        predicatesWithVariablesList=[]
        #the state dictionary has all the information specific to predicates
        StateDictionary = {}
        #the actionDictionary has all the names of the actions as key alongwith the relevant predicates to those actions as values
        actionDictionary = {}
        predicateTypeDictionary,informationConstraintCount,predicateTypeDictionaryPerAction, operatorDictionary={},{},{},{}
        compare = lambda x, y: collections.Counter(x) == collections.Counter(y)
        count=0
        inreader=csv.reader(csvFile, delimiter=';')
        for row in inreader:
            count=count+1
            if(count > NUMRECORDS):break
            #keep track of the row number being read
            print('on row number: ', count)
            #the following four lines allow the splitting of the action sequences, initial state, final state and action-state interleaved sequences
            splitActions=(re.search('\[\s?(.*),?\s?\]', row[0], re.DOTALL)).group(1).split(',')#split each action in to its action name and arguments
            splitInitialState=(re.search('\[\s?(.*)\]', row[1], re.DOTALL)).group(1).split(',')
            splitFinalState=(re.search('\[\s?(.*)\]', row[2], re.DOTALL)).group(1).split(',')
            splitOperatorStateSequence=(re.search('\[\s?(.*)\]', row[3], re.DOTALL)).group(1).split(',')
            #generalize for state-action sequence to build information constraints
            for j in range(0,len(splitOperatorStateSequence)):
                splitActionToArguments=splitOperatorStateSequence[j].split()
                match=re.search('[operator|state]:\s?\(?(\w+(-\w+)*)\s', splitOperatorStateSequence[j], re.DOTALL)                
                propositionType=['operator' if 'operator' in splitOperatorStateSequence[j] else 'state']
                #to inject an information constraint, the first action must be taken into account. Take into account the first action of every trace
                if(match):
                    #if we are considering the first action of the trace, then it must be accounted for                    
                    if(j==0):
                        firstActionInTrace=match.group(1)
                    for k in range(2,len(splitActionToArguments)):
                        #the search is modified to findall to accommodate multiple '-' characters that can be found                         
                        matchAction=re.findall('-(\w+)', splitActionToArguments[k], re.DOTALL)
                        #this is changed to matchAction[-1] to accommodate multiple '-' characters that can be found
                        if(matchAction[-1]):                            
                            actionArgumentsList.append(matchAction[-1])
                    if(count<=30):
                        indexValue=tempActionPredicateList.index(match.group(1)) if (match.group(1)) in tempActionPredicateList else -1
                        if(indexValue!=-1):
                            tempMatchList=findVariablesSameType(operatorDictionary[match.group(1)],actionArgumentsList,sameTypeList)
                        else:  
                            tempActionPredicateList.append(match.group(1)) 
                    operatorDictionary[match.group(1)]=actionArgumentsList#add actions to dictionary
                    if(count<=30 and propositionType[0]=='operator'):                    
                        if match.group(1) not in operatorAllCombinationsDictionary.keys(): 
                            operatorAllCombinationsDictionary[match.group(1)] =  [actionArgumentsList]
                        else:
                            operatorAllCombinationsDictionary[match.group(1)] = operatorAllCombinationsDictionary[match.group(1)] + [actionArgumentsList]
                    unitaryOperatorList.append(propositionType[0])
                    unitaryOperatorList.append(match.group(1))
                    unitaryOperatorList.append(actionArgumentsList)
                    #sequentially add the operators/predicates with their argument types as they appear in the list. The order is important to impose the information constraint
                    operatorSequenceList.append(unitaryOperatorList)
                actionArgumentsList=[]
                unitaryOperatorList=[]
            for key in operatorAllCombinationsDictionary.keys():
                b_set = set(tuple(x) for x in operatorAllCombinationsDictionary[key])
                operatorAllCombinationsDictionary[key] = [ list(x) for x in b_set ]
            tempOperatorSequenceList = copy.copy(operatorSequenceList)
            i = 0
            tempActionPredicateList = []
            while i < len(operatorSequenceList):
                tracker = 0
                belongs = False
                operatorSequenceOuterList, operatorSequenceInnerList = [],[]
                operatorIndices = [j for j, x in enumerate(operatorSequenceList) if x[0] == 'operator']
                if operatorSequenceList[i][0]=='state':
                    predicateIndices = [j for j, x in enumerate(operatorSequenceList) if (x[1] == operatorSequenceList[i][1] and x[2] == operatorSequenceList[i][2])]
                    #run a loop for each of these indices and add operators corresponding to predicates
                    for l in predicateIndices:
                        for k in operatorIndices:
                            if(l>k):
                                testNew = []
                                for tt in operatorAllCombinationsDictionary[operatorSequenceList[k][1]]:
                                    testNew.append([m for l, m in enumerate(tt) if (m in operatorSequenceList[i][2])])
                                for element in testNew:
                                    tempI = copy.deepcopy(operatorSequenceList[i][2])
                                    tempI.sort()
                                    element.sort()
                                    if(element == tempI):
                                        belongs = True
                                        break
                                if(belongs):
                                    belongs = False
                                    #create new structure which takes into account all the new operators which it has seen before and takes into account the delete constraint for the last operator in the sequence too
                                    operatorSequenceInnerList.append(operatorSequenceList[k][1])                                    
                                    if((operatorSequenceList[i][1]+str(operatorSequenceList[i][2])) not in predicateInformationConstraintDict.keys()):
                                        #add only predicates which are applicable to a particular action, after comparing with the operatorAllCombinationsDictionary
                                        predicateInformationConstraintDict[operatorSequenceList[i][1]+str(operatorSequenceList[i][2])] = [operatorSequenceList[k][1]]
                                        #maintain a count of all the individual predicate types seen in the trace
                                        predicateInformationConstraintDictCount[operatorSequenceList[i][1]+str(operatorSequenceList[i][2])] = 1                                         
                                    else:
                                        predicateInformationConstraintDict[operatorSequenceList[i][1]+str(operatorSequenceList[i][2])] = list(set(predicateInformationConstraintDict[operatorSequenceList[i][1]+str(operatorSequenceList[i][2])]+ [operatorSequenceList[k][1]]))
                                        predicateInformationConstraintDictCount[operatorSequenceList[i][1]+str(operatorSequenceList[i][2])] = predicateInformationConstraintDictCount[operatorSequenceList[i][1]+str(operatorSequenceList[i][2])] + 1
                            #save for a set of constraints which are determined if the predicate appears before or after the execution of the action
                            #If the predicate is before the action, it may belong to the prelist of the action
                            #If the predicate is after the action, it may belong to the effects of the action
                            elif(l<k):
                                testNew = []
                                for tt in operatorAllCombinationsDictionary[operatorSequenceList[k][1]]:
                                    testNew.append([m for l, m in enumerate(tt) if (m in operatorSequenceList[i][2])])
                                for element in testNew:
                                    tempI = copy.deepcopy(operatorSequenceList[i][2])
                                    tempI.sort()
                                    element.sort()
                                    if(element == tempI):
                                        belongs = True
                                        break
                                if(belongs):
                                    belongs = False
                                    if((operatorSequenceList[i][1]+str(operatorSequenceList[i][2])) not in predicateBeforeConstraintDict.keys()):
                                        #add only predicates which are applicable to a particular action, after comparing with the operatorAllCombinationsDictionary
                                        predicateBeforeConstraintDict[operatorSequenceList[i][1]+str(operatorSequenceList[i][2])] = [operatorSequenceList[k][1]]
                                    else:
                                        predicateBeforeConstraintDict[operatorSequenceList[i][1]+str(operatorSequenceList[i][2])] = list(set(predicateBeforeConstraintDict[operatorSequenceList[i][1]+str(operatorSequenceList[i][2])]+ [operatorSequenceList[k][1]]))
                            else:break
                        #put in type checking mechanism where only predicates that can match the signature of the final action can be applied to that action as a constraint
                        lastActionMatch = False
                        for tempSingleActionList in operatorAllCombinationsDictionary:                
                            indices = [l for l, m in enumerate(operatorSequenceList[i][2]) for tt in tempSingleActionList if m==tt]
                            if(len(list(set(indices)))==len(operatorSequenceList[i][2])):
                                lastActionMatch = True
                                break
                        if(lastActionMatch):
                            if((operatorSequenceList[i][1]+str(operatorSequenceList[i][2])) not in predicateInformationConstraintDelDict.keys()):
                                predicateInformationConstraintDelDict[operatorSequenceList[i][1]+str(operatorSequenceList[i][2])] = [operatorSequenceList[operatorIndices[len(operatorIndices)-1]][1]]
                            else:
                                predicateInformationConstraintDelDict[operatorSequenceList[i][1]+str(operatorSequenceList[i][2])] = list(set(predicateInformationConstraintDelDict[operatorSequenceList[i][1]+str(operatorSequenceList[i][2])]+ [operatorSequenceList[operatorIndices[len(operatorIndices)-1]][1]]))
                        lastActionMatch = False
                        if(operatorSequenceInnerList!=[]):
                            operatorSequenceOuterList.append(list(set(operatorSequenceInnerList)))        
                        operatorSequenceInnerList = []
                    operatorInformationConstraintDictionary[operatorSequenceList[i][1]+str(operatorSequenceList[i][2])] = [tt for tt in map(list, set(map(tuple, operatorSequenceOuterList)))]
                    operatorSequenceOuterList = []   
                    #add the constraint information to the state dictionary                                    
                    if (operatorSequenceList[i][1] not in StateDictionary.keys()):
                        StateDictionary[operatorSequenceList[i][1]] = [operatorSequenceList[i][2]]
                    else:
                        StateDictionary[operatorSequenceList[i][1]] = StateDictionary[operatorSequenceList[i][1]]+[operatorSequenceList[i][2]]
                    for x in predicateIndices:
                        operatorSequenceList.pop(x-tracker)
                        tracker =  tracker + 1
                    i = 0
                i = i+1                                                           
                                    
            tempActionPredicateList = []
            for j in range(0,len(splitActions)):
                splitActionToArguments=splitActions[j].split()
                match=re.search('\((.*)', splitActionToArguments[0], re.DOTALL)
#to inject an information constraint, the first action must be taken into account. Take into account the first action of every trace
                if(match):
#if we are considering the first action of the trace, then it must be accounted for                    
                    if(j==0):
                        firstActionInTrace=match.group(1)
                    #here is a good place to add code for finding predicates of the same type
                    #if match.group() already exists in the list which consists of all the actions
                    for k in range(1,len(splitActionToArguments)):
                        matchAction=re.findall('-(\w+)', splitActionToArguments[k], re.DOTALL)
                        #code being added as matchAction isnt always found
                        if not matchAction[-1]:continue
                        actionArgumentsList.append(matchAction[-1])
                    if(count<=10):
                        indexValue=tempActionPredicateList.index(match.group(1)) if (match.group(1)) in tempActionPredicateList else -1
                        if(indexValue!=-1):
                       #find match between actionDictionary[match.group()] and splitActionToArguments
                            tempMatchList=findVariablesSameType(actionDictionary[match.group(1)],actionArgumentsList,sameTypeList)
                        else:  
                            tempActionPredicateList.append(match.group(1)) 
                    
                    actionDictionary[match.group(1)]=actionArgumentsList#add actions to dictionary
#                     target.write(match.group(1))
#                     target.write(" ")
                    ##LOGIC FOR WRITING THE FILE FOR THE TRULEGROWTH SHOULD BE PUT HERE
                actionArgumentsList=[]
#            target.write('-2')
#             target.write('\n')
            for j in range(0,len(splitInitialState)):
                signatureMatch = False
#                 informationConstraintDictionaryInner = {}
                splitInitialStateToArguments=splitInitialState[j].split()
                match=re.search('\((\w+(-\w+)*)', splitInitialStateToArguments[0], re.DOTALL)
                for k in range(1,len(splitInitialStateToArguments)):
                    matchPred=False
                    matchPredInner = False
                    matchState=re.findall('-(\w+)', splitInitialStateToArguments[k], re.DOTALL)
                    #continue statement is added here to ensure the code does not block in case a match isnt found
                    if not matchState[-1]:continue
                    stateArgumentsList.append(matchState[-1])
#the predicate and arguments need to be matched against the action to see if the predicates are applicable to the actions or not                    
                for tempSingleActionList in operatorAllCombinationsDictionary[firstActionInTrace]:                
                    indices = [l for l, m in enumerate(stateArgumentsList) for tt in tempSingleActionList if m==tt]
                    if(len(list(set(indices)))==len(stateArgumentsList)):
                        signatureMatch = True
                if(match):
#                    StateDictionary[match.group(1)]=stateArgumentsList
                    if (match.group(1) not in StateDictionary.keys()):
                        StateDictionary[match.group(1)]=[stateArgumentsList]
                    else:
                        for k in StateDictionary[match.group(1)]:
                            if (compare(k,stateArgumentsList)):
                                matchPred=True
                                break
                        if not(matchPred):
                            StateDictionary[match.group(1)].append(stateArgumentsList)
#if predicate is applicable to the action then the predicate must be added to the informationConstraintDictionary
                    if(signatureMatch):
                        if (match.group(1) not in informationConstraintDictionary[firstActionInTrace].keys()):
                            informationConstraintDictionary[firstActionInTrace][match.group(1)]=[stateArgumentsList]
                        else:
                            for k in informationConstraintDictionary[firstActionInTrace][match.group(1)]:
                                if (compare(k,stateArgumentsList)):
                                    matchPredInner=True
                                    break
                            if not(matchPredInner):
                                informationConstraintDictionary[firstActionInTrace][match.group(1)].append(stateArgumentsList)
                stateArgumentsList=[]
            for j in range(0,len(splitFinalState)):
                matchPred=False
                splitFinalStateToArguments=splitFinalState[j].split()
                match=re.search('\((.*)', splitFinalStateToArguments[0], re.DOTALL)
                for k in range(1,len(splitFinalStateToArguments)):
                    matchState=re.findall('-(\w+)', splitFinalStateToArguments[k], re.DOTALL)
                    if not matchState[-1]: continue
                    stateArgumentsList.append(matchState[-1])
                if(match):
                    if (match.group(1) not in StateDictionary.keys()):
                        StateDictionary[match.group(1)]=[stateArgumentsList]
                    else:
                        for k in StateDictionary[match.group(1)]:
                            if (compare(k,stateArgumentsList)):
                                matchPred=True
                                break
                        if not(matchPred):
                            StateDictionary[match.group(1)].append(stateArgumentsList)
                stateArgumentsList=[]
            #whatever predicates we find in the predicateInformationConstraintDict should be added to the stateDictionary 
            for x in predicateInformationConstraintDict.keys():
                match =  re.search(r'(\w+(-\w+)*)\[(.*)\]',x)              
                predicateName = match.group(1)
                arguments = match.group(3)
                if predicateName not in StateDictionary.keys():
                    StateDictionary[predicateName] = arguments
            if not firstActionInTrace in informationConstraintCount.keys():
                informationConstraintCount[firstActionInTrace]=1
            else:
                informationConstraintCount[firstActionInTrace]=informationConstraintCount[firstActionInTrace]+1
        #eliminate duplicates from the informationConstratintList and the StateDictionary
        for key in StateDictionary.keys():
            StateDictionary[key] = [i for i in map(list, set(map(tuple, StateDictionary[key])))]
    
        #Predicates to be added in the models to be added here
        #Find the predicates which are compatible with each action
        #figure out if all the elements of one list can be part of another list or not
        predicatesForSameAction=[]
        tempIndiceOuterList = []
        global parentArgumentDictionary
        predicateActionMatchDictionary={}
        #in the tempMatchList, group all the arguments which can be replaced with one another
        for i in tempMatchList:
            tempIndiceInnerList = []
            listAddition = False
            indices = [j for j, x in enumerate(tempMatchList) if x == i]
            indices = [j+1 if j%2==0 else j-1 for j in indices]
            tempIndiceInnerList.append(i)
            for t in indices:
                tempIndiceInnerList.append(tempMatchList[t])
            #figure out if this inner list already exists in the outer list
            for x in tempIndiceOuterList:
                x.sort()
                tempIndiceInnerList.sort()
                if (set(x).issubset(set(tempIndiceInnerList))):
                    tempIndiceOuterList.remove(x)
                    listAddition = True
                    break
                elif(set(tempIndiceInnerList).issubset(set(x))):
                    listAddition = False
                    break
                else:
                  listAddition = True
            if(listAddition or tempIndiceOuterList == []): 
                tempIndiceOuterList.append(tempIndiceInnerList) 
                listAddition = False
        #change the parent argument dictionary to integrate the changes made above
        for i in tempIndiceOuterList:
            key = ''
            for j in i:
                key = key+ j[0] 
            parentArgumentDictionary[key] = i        
        for key in actionDictionary:
            singleActionList=actionDictionary[key]
            for stateKey in StateDictionary:
                tempSingleActionList=singleActionList[:]
                predicateTypeList=[]
                predicatesForSameActionTemp=[]
                matchedVariables=[]
                singleStateList=StateDictionary[stateKey]
                instantiatedVariablesPerAction=[]
                tempSingleStateList=copy.deepcopy(singleStateList)
                variableTypePerPredicate=[]
                for k in tempSingleStateList:
                    matchedVariablesInner, allMatchedVariables=[],[]
                    tempTempSingleActionList=copy.deepcopy(tempSingleActionList)
                    for r in k:
                        intersectionElements=set(tempTempSingleActionList).intersection(set([r]))
                        if(len(intersectionElements)!=0):
                            matchedVariablesInner.append(list(intersectionElements)[0])
                            tempTempSingleActionList.remove(list(intersectionElements)[0])
                    #make 3 cases for no match, partial and complete match
                    #case when subset of the variables is repeated and multiple matches can be created needs to be added
                    t=copy.copy(k)
                    if(matchedVariablesInner!=[] and len(matchedVariablesInner)==len(k)):
                        allMatchedVariables.append(matchedVariablesInner)

                    #find the max frequency of all the variables in all the combinations of the actions
                    objectFrequencyDictionary, elementFrequencyDictionary = {}, {}
                    for i in operatorAllCombinationsDictionary[key]:
                        elementFrequencyDictionary = collections.Counter(i)
                        if (objectFrequencyDictionary =={}): objectFrequencyDictionary = elementFrequencyDictionary
                        else:
                            for j in elementFrequencyDictionary.keys():
                                if j in objectFrequencyDictionary.keys():
                                    objectFrequencyDictionary[j] = max(elementFrequencyDictionary[j], objectFrequencyDictionary[j])
                    changedArgumentList = []
                    if((len(matchedVariablesInner)==0 or len(k)>len(matchedVariablesInner)) and changedArgumentList==[]):#case when no match is found
                        continue
                    for i in changedArgumentList:
                        if i!=[] and i not in allMatchedVariables:
                            allMatchedVariables.append(i)
                    predicateFrequencyList=dict()
                    instantiatedVariablesPerAction=[]
                    t=i
                    c=0
                    for matchedVariables in allMatchedVariables:
                        tempTempSingleActionList=tempSingleActionList[:]
                        for tt in matchedVariables:
                            predicateFrequencyList[tt]=objectFrequencyDictionary[tt]
                        tempTempSingleActionList=tempSingleActionList[:]
                        for j in range(max(predicateFrequencyList.values())):
                            tempInstantiatedVariablesPerAction=[]
                            tempVariableTypePerPredicate=[]
                            if(len(set(matchedVariables))!=len(matchedVariables)):
                                t=0
                                for i in list(matchedVariables):
                                    tempInstantiatedVariablesPerAction.append(list(i)[0]+list(i)[1]+str(t)+" - "+i)
                                    tempVariableTypePerPredicate.append(i)
                                    t=t+1
                                instantiatedVariablesPerAction.append(tempInstantiatedVariablesPerAction)
                                indices = [l for l, m in enumerate(variableTypePerPredicate) for i in tempVariableTypePerPredicate if m==i]
                                if( not indices):                            
                                    variableTypePerPredicate.append(tempVariableTypePerPredicate)
                                    variableTypePerPredicate.append(tempInstantiatedVariablesPerAction)   
                                break
                            for i in list((matchedVariables)):
                                if(j>0 and j==predicateFrequencyList[i]):
                                    tempInstantiatedVariablesPerAction.append(list(i)[0]+list(i)[1]+str(j-1)+" - "+i)
                                    tempVariableTypePerPredicate.append(i)
                                else:
                                    tempInstantiatedVariablesPerAction.append(list(i)[0]+list(i)[1]+str(j)+" - "+i)
                                    tempVariableTypePerPredicate.append(i)
                            instantiatedVariablesPerAction.append(tempInstantiatedVariablesPerAction)
                            #add it alternatively in order to mantain order in the case of the constraint creation step which comes after 
                            variableTypePerPredicate.append(tempVariableTypePerPredicate)
                            variableTypePerPredicate.append(tempInstantiatedVariablesPerAction)
   
                    for j in instantiatedVariablesPerAction:
                        predicatesForSameAction.append(str(str(stateKey)+str(j)))
                    predicateTypeDictionaryPerAction[stateKey]=variableTypePerPredicate
                    #add dictionary to save the names and variable types of the actions                            
                    predicatesWithVariablesList.append(str(str(stateKey)+str(variableTypePerPredicate)))
                    predicatesForSameActionTemp.append(str(str(stateKey)+str(instantiatedVariablesPerAction)))
                    if(len(predicateTypeList)!=0):
                        predicatesWithVariablesList.append(predicateTypeList)
                    tempSingleActionList=singleActionList[:]       
                    predicateActionMatchDictionary[key]=(predicatesForSameAction)
                    predicateTypeDictionary[key]=predicateTypeDictionaryPerAction
            predicatesForSameAction=[]
            predicatesWithVariablesList=[]
            predicateTypeDictionaryPerAction={}
        #Till now there are all predicates which can be applied to a single action, but this includes possible duplicates. Introduce a functio`n which will eliminate the predicates which are too bigger than each other
        modelAllConstraints(predicateActionMatchDictionary, actionDictionary, StateDictionary, predicateTypeDictionary, informationConstraintDictionary, informationConstraintCount, parentArgumentDictionary, operatorAllCombinationsDictionary)
        

# this method replaces the objects found in the traces with their diectic references  
def matchAndReplacePredicates(mainlist):
    global MINING_ALGO, NUMRECORDS
    with open(str(ABSPATH) + '/generatedFiles/labelledTraces.data','w') as csvFile, open (DATA_FILE,"r") as csvReaderFile:        
        print('in method matchAndReplacePredicates')
        inreader = csv.reader(csvReaderFile, delimiter=';')
        outwriter=csv.writer(csvFile,delimiter=';')
        count=0
        for row in csvReaderFile:
            if(count > NUMRECORDS):break
            rowList=[]
            splitString = row.split(';')
            for columnString in splitString:
                if(columnString.strip()!=''):
                    for j in range(len(mainlist)):
                        if(j%2!=0):
                            for k in range(len(mainlist[j])):
                                columnString=''.join(columnString)
                                mainlist[j][k]=''.join(mainlist[j][k])
                                mainlist[j-1]=''.join(mainlist[j-1])
                                columnString = columnString.replace(mainlist[j][k]+' ',mainlist[j][k]+'-'+mainlist[j-1]+' ').replace(mainlist[j][k]+')',mainlist[j][k]+'-'+mainlist[j-1]+')')
                    csvFile.write(columnString+';')
            csvFile.write('\n')
            count = count + 1
    csvFile.close()
    csvReaderFile.close()
    scanTraces()
    
    
# this method is used to read the object specific information from the problem file. All the
# data contained within the ':objects' tag is picked up, principally the variable names and their types
# Write this information to a csv file for further reference    
def readPredicates():
#     if not os.path.exists("createdFiles"): os.makedirs("createdFiles")
    with open(PROBLEM_FILE,"r") as infile, open(str(ABSPATH) + "/generatedFiles/predicates.csv","w") as csvfile:                        
            print('in method readPredicates()')
            newline = infile.read();
            match=re.search(r'\(:objects\n.*?\)', newline, re.DOTALL)
            csvfile.write(match.group(0).lower())
    infile.close();
    csvfile.close();
    separateObjects();
    
    
def separateObjects():
    with open(str(ABSPATH) + "/generatedFiles/predicates.csv", "r") as csvfile, open(str(ABSPATH) + "/generatedFiles/objects.csv", "w") as objectsFile:
        print('in method separateObjects')
        global mainlist
        inreader = csv.reader(open(str(ABSPATH) + "/generatedFiles/predicates.csv","r"))
        for row in inreader:
            rowString=','.join(row)
            match=re.search(r'(.*)- (\w*)', rowString)
            if match:
                sublist=[]
                arguments=match.group(1)
                types=match.group(2)
                argumentsAsString=arguments.split()
                mainlist.append(types)
                present=False
                for j in argumentsAsString:
                    if(len(sublist)==0):
                        sublist.append(j)
                    else:
                        for k in (sublist):
                            if (k in j):
                                present=True
                                break
                        if not(present):
                            sublist.append(j)
                mainlist.append(sublist)
    csvfile.close()
    objectsFile.close()
    matchAndReplacePredicates(mainlist)   
    
def main(): 
    global DATA_FILE, PROBLEM_FILE, DOMAIN_FILE, INFORMATION_CONSTRAINT1, INFORMATION_CONSTRAINT2, ACTION_CONSTRAINT1, ACTION_CONSTRAINT2, DOMAIN_NAME, MINING_ALGO, SOLVER, NUMRECORDS, MINING_CONFIDENCE, MINING_SUPPORT, ABSPATH
    DOMAIN_NAME = sys.argv[1]   
    DATA_FILE = str(ABSPATH) + '/' + sys.argv[2]
    NUMRECORDS = int(sys.argv[3])
    PROBLEM_FILE = str(ABSPATH) + '/' + sys.argv[4]
    DOMAIN_FILE = str(ABSPATH) + '/' + str(sys.argv[5])
    #weight of the first and second information constraints
    INFORMATION_CONSTRAINT1 = sys.argv[6]
    INFORMATION_CONSTRAINT2 = sys.argv[7]
    #weight of the first and second action constraints    
    ACTION_CONSTRAINT1 = sys.argv[8]
    ACTION_CONSTRAINT2 = sys.argv[9]
    MINING_ALGO = str(sys.argv[10])
    MINING_CONFIDENCE = float(sys.argv[11])
    MINING_SUPPORT = float(sys.argv[12])
    #choose between the maxsat solver and the maxwalksat solver    
    SOLVER = sys.argv[13]
    readPredicates();
    
if __name__ == "__main__":
    #start to calculate running time of the program
    startTime = timeit.default_timer()
    ABSPATH = os.getcwd()
    print(ABSPATH)
    main()
    